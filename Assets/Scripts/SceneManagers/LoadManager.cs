﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadManager : MonoBehaviour
{
    public Text text;
    [SerializeField] private SOGame gameSettings;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("Score"))
            gameSettings.score = PlayerPrefs.GetInt("Score");
        StartCoroutine(GoToGame());
    }

    private IEnumerator GoToGame()
    {
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(0.2f);
            string s = "";
            for (int j = 0; j <= i; j++)
                s += ".";
            text.text = "Load"+s;
        }
        GameSceneManager.Instance.LoadStartScene();
    }
}
