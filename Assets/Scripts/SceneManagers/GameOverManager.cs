﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private SOGame gameSettings;
    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score: " + gameSettings.score;
    }
}
