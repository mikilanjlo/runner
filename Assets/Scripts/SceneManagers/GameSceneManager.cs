﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    public static GameSceneManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void LoadGameOverScene()
    {
        Debug.Log("gameover");
        SceneManager.LoadScene("GameOverScene");
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void LoadLoadScene()
    {
        SceneManager.LoadScene("LoadScene");
    }


    public void ExitGame()
    {
        Application.Quit();
    }
}
