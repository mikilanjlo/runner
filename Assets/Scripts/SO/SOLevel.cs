﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Level", menuName = "Level")]
public class SOLevel : ScriptableObject
{
    //[SerializeField] public int minPlayerPosition;
    //[SerializeField] public int maxPlayerPosition;
    [SerializeField] public int stepsCount;
    [SerializeField] public float minVehicleSpeed;
    [SerializeField] public float maxVehicleSpeed;
    [SerializeField] public float minLogSpeed;
    [SerializeField] public float maxLogSpeed;
    [SerializeField] public float minVehicleSpawnSpeed;
    [SerializeField] public float maxVehicleSpawnSpeed;
    [SerializeField] public float minLogSpawnSpeed;
    [SerializeField] public float maxLogSpawnSpeed;
}
