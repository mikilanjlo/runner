﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Terrain  Data", menuName = "Terrain Data/ with vehicle Alternate Side")]
public class SOTerrainWithVehicle : SOTerrain
{
    [SerializeField] private List<AVihicleSpawner> PossibleTerrainWithVehicle;
    private bool isRightSide = false;

    public override GameObject GetTerrain()
    {
        List<AVihicleSpawner> vehicleTerrains = PossibleTerrainWithVehicle.Where(x => x.IsRightSide == isRightSide).ToList();
        isRightSide = !isRightSide;
        return vehicleTerrains[Random.Range(0, vehicleTerrains.Count)].gameObject;
    }
}
