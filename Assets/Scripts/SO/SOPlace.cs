﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPlaceConfig", menuName = "Place prefab/new place config")]
public class SOPlace : ScriptableObject
{
    [SerializeField]
    public int steps;
}
