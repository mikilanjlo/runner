﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Terrain Data", menuName = "Terrain Data/ Simple")]
public class SOTerrain : ScriptableObject
{
    [SerializeField] public List<GameObject> PossibleTerrain;
    //[SerializeField] private List<SOLevel> levels;
    //private static int step = 0;
    public int maxInSuccesion;

    public virtual GameObject GetTerrain()
    {
        //step++;
        //if (step > levels[0].stepsCount)
        //{
        //    levels.RemoveAt(0);
        //    step = 0;
        //}
        return PossibleTerrain[Random.Range(0, PossibleTerrain.Count)];
    }


    //public SOLevel GetLevel()
    //{       
    //    return levels[0];
    //}

}
