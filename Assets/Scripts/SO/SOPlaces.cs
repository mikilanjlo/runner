﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPlacesConfig", menuName = "Places prefab/new places config")]
public class SOPlaces : ScriptableObject
{
    [SerializeField]
    public GameObject[] places;
}
