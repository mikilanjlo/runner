﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Map", menuName = "Map")]
public class SOMap : ScriptableObject
{
    [SerializeField] public List<GameObject> terrains;
}
