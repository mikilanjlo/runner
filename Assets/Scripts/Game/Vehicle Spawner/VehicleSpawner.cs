﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSpawner : AVihicleSpawner
{
    protected override void SetSpawnerTime()
    {
        SOLevel currentLevel = levelManager.levels[levelManager.currentlevel];
        base.SetSpawnerTimeWithLevelManager(currentLevel.minVehicleSpawnSpeed, currentLevel.maxVehicleSpawnSpeed);
    }

    protected override void SetSpeed()
    {
        SOLevel currentLevel = levelManager.levels[levelManager.currentlevel];
        base.SetSpeedWithLevelManager(currentLevel.minVehicleSpeed, currentLevel.maxVehicleSpeed);
    }

    
}
