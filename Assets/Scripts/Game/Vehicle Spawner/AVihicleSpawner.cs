﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AVihicleSpawner : MonoBehaviour
{
    #region Fields
    [SerializeField] private GameObject vehicle;
    [SerializeField] private Transform startSpawnPos;
    [SerializeField] private Transform endSpawnPos;
    [SerializeField] private float minSeparationTime;
    [SerializeField] private float maxSeparationTime;
    [SerializeField] private bool isRightSide;
    protected LevelManager levelManager;
    protected float speed ;
    private float distanceBetweenSpawn;
    [SerializeField] protected float spawnTime;
    #endregion


    public bool IsRightSide { get => isRightSide;  }
    public float Speed {
        get
        {
            return speed;
        }
    }

    void Start()
    {
        levelManager = LevelManager.instance;
        
        SetSpeed();
        vehicle.transform.GetComponent<AMovingObject>().speed = speed;
        SetSpawnerTime();
        distanceBetweenSpawn = 0;
        StartCoroutine(ReductionDistanceBetweenSpawn());
        StartCoroutine(SpawnVehicle());
    }

    protected abstract void SetSpeed();

    protected abstract void SetSpawnerTime();

    protected  void SetSpeedWithLevelManager(float min, float max)
    {
        SOLevel currentLevel = levelManager.levels[levelManager.currentlevel];
        float difference = (max - min);

        float OnePercent = (difference / currentLevel.stepsCount);

        speed = min +
            OnePercent *
            levelManager.CurrentStep;
    }

    protected  void SetSpawnerTimeWithLevelManager(float min, float max)
    {
        SOLevel currentLevel = levelManager.levels[levelManager.currentlevel];
        float difference = (max - min);

        float OnePercent = (difference / currentLevel.stepsCount);

        spawnTime = Random.Range(min,
            max -
            OnePercent *
            levelManager.CurrentStep);
    }

    protected virtual IEnumerator SpawnVehicle()
    {
        while (true)
        { 
            GameObject go =Instantiate(vehicle, Vector3.Lerp(startSpawnPos.position, endSpawnPos.position,distanceBetweenSpawn), Quaternion.identity);
            go.transform.parent = transform;
            if (!IsRightSide)
                go.transform.Rotate(new Vector3(0, 180, 0));
            yield return new WaitForSeconds(spawnTime);
        }
    }

    private IEnumerator ReductionDistanceBetweenSpawn()
    {
        while(distanceBetweenSpawn < 1)
        {
            distanceBetweenSpawn += 0.1f;
            yield return new WaitForSeconds(1f);
        }
        distanceBetweenSpawn = 1;
    }

    
}
