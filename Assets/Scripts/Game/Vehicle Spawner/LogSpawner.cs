﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogSpawner : AVihicleSpawner
{


    protected override void SetSpawnerTime()
    {
        SOLevel currentLevel = levelManager.levels[levelManager.currentlevel];
        base.SetSpawnerTimeWithLevelManager(currentLevel.minLogSpawnSpeed, currentLevel.maxLogSpawnSpeed);
    }

    protected override void SetSpeed()
    {
        SOLevel currentLevel = levelManager.levels[levelManager.currentlevel];
        base.SetSpeedWithLevelManager(currentLevel.minLogSpeed, currentLevel.maxLogSpeed);
    }

}
