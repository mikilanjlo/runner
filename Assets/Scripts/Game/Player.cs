﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Fields
    [SerializeField] TerainGenerator terainGenerator;
    [SerializeField] private float minSwaipDistance;
    [SerializeField] private Camera cam;
    [SerializeField] private float maxDistanceFromCenter;
    
    private Animator animator;
    private bool isMoving;
   
    
    private Vector3 fp;
    private Vector3 sp;
    private KeyCode? vhod = null;
    #endregion


    #region Unity Functions
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(transform.position.z) > maxDistanceFromCenter + 2)
            Destroy(gameObject);
        if (Input.GetMouseButtonDown(0))
        {
            fp = cam.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            sp = cam.ScreenToWorldPoint(Input.mousePosition);
            if (Mathf.Abs(fp.y - sp.y) < minSwaipDistance && Mathf.Abs(fp.x - sp.x) < minSwaipDistance)
            {
                vhod = KeyCode.W;
            }
            else
            {
                Debug.Log("mouse " + Vector2.Angle(new Vector2(1,0), new Vector2(fp.x - sp.x, fp.y - sp.y)));
                if(fp.y < sp.y && Mathf.Abs(fp.y - sp.y) > minSwaipDistance &&
                     Vector2.Angle(new Vector2(0, -1), new Vector2(fp.x - sp.x, fp.y - sp.y)) < 45)
                {
                    vhod = KeyCode.Space;
                }
                else if(fp.x > sp.x && Mathf.Abs(fp.x - sp.x) > minSwaipDistance &&
                     Vector2.Angle(new Vector2(1, 0), new Vector2(fp.x - sp.x, fp.y - sp.y)) < 45)
                {
                    vhod = KeyCode.A;
                }
                else if(fp.x < sp.x && Mathf.Abs(fp.x - sp.x) > minSwaipDistance &&
                     Vector2.Angle(new Vector2(-1, 0), new Vector2(fp.x - sp.x, fp.y - sp.y)) < 45)
                {
                    vhod = KeyCode.D;
                }
            }
        }
        if ((Input.GetKeyDown(KeyCode.W) || vhod == KeyCode.W)&& !isMoving)
        {
            float zDifference = 0;
            if(transform.position.z % 1 != 0)
            {
                zDifference = Mathf.Round(transform.position.z) - transform.position.z;
            }
            StartCoroutine( Move(new Vector3(1, 0, zDifference),true));
            Debug.Log(transform.position);
        }
        else if((Input.GetKeyDown(KeyCode.A) || vhod == KeyCode.A) && !isMoving)
        {
            //transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
            MoveCharacter(new Vector3(0, 0, 1), false);
        }
        else if ((Input.GetKeyDown(KeyCode.D) || vhod == KeyCode.D) && !isMoving)
        {
            //transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            MoveCharacter(new Vector3(0, 0, -1),false);
            
        }
        else if ((Input.GetKeyDown(KeyCode.Space) || vhod == KeyCode.Space) && !isMoving)
        {
            GameObject space = new GameObject(); 
            space.transform.position = transform.position + new Vector3(1.5f, 0, 0);
            transform.SetParent(space.transform);
            StartCoroutine(Rotate(transform.position + new Vector3(2, 0, 0), space));
        }
        vhod = null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        AMovingObject vehicle = collision.collider.GetComponent<AMovingObject>();
        if ( vehicle != null)
        {
            if (vehicle.isLog)
            {
                transform.parent = collision.collider.transform;
            }
        }
        else
        {
            transform.parent = null;
        }
    }

    #endregion

    #region Private Functions
    private void MoveCharacter(Vector3 difference, bool animate)
    {
        if (animate)
        {
            animator.SetTrigger("hop");
            isMoving = true;
        }
        if (Mathf.Abs((transform.position + difference).z) < maxDistanceFromCenter)
        {
            gameObject.transform.position = (transform.position + difference);
            terainGenerator.SpawnTerrain(transform.position);
        }

        //transform.Translate(difference);
    }

    

    private IEnumerator Move(Vector3 difference, bool animate)
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        isMoving = true;
        Vector3 startPos = transform.position;
        Vector3 endPos = startPos + difference;
        float time = 0.3f;
        float currentTime = 0;
        if (Mathf.Abs((transform.position + difference).z) < maxDistanceFromCenter)
        {
            while (currentTime <= time)
            {
                gameObject.transform.position = Vector3.Lerp(startPos, endPos, currentTime / time);
                currentTime += Time.deltaTime;
                yield return null;
            }
            gameObject.transform.position = endPos;
            terainGenerator.SpawnTerrain(transform.position);
            FinishHop();
        }
    }

    private IEnumerator Rotate(Vector3 nextPos, GameObject space)
    {
        isMoving = true;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
        //space.transform.eulerAngles = new Vector3(0, 0, 360);
        while (space.transform.eulerAngles.z >= 180 || space.transform.eulerAngles.z == 0)
        {
            space.transform.Rotate(new Vector3(0, 0, -5f));
            Debug.Log(space.transform.eulerAngles.z);
            yield return null;
        }
        transform.SetParent(null);
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        transform.position = nextPos;
        
        Destroy(space);
        terainGenerator.SpawnTerrain(transform.position);
        terainGenerator.SpawnTerrain(transform.position);
        isMoving = false;
    }
    #endregion

    #region Public Functions
    public void FinishHop()
    {
        isMoving = false;
        Debug.Log("finish");
        //transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }
    #endregion
}
