﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AMovingObject : MonoBehaviour
{
    [SerializeField] public float speed = 0;
    [SerializeField] protected int maxDistanceFromStart;
    [SerializeField] private Vector3 startPos;
    [SerializeField] public bool isLog;

    private void Start()
    {
        //speed = 0;
        startPos = transform.position;
        maxDistanceFromStart = 50;
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        if (Mathf.Abs(startPos.z - transform.position.z) > maxDistanceFromStart)
            Destroy(gameObject);
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    

    public void Initialize(int maxDistanceFromStart,Vector3 startPos)
    {
        if (maxDistanceFromStart == 0)
            throw new System.ArgumentException("Max Distance From Start equel 0");
        this.maxDistanceFromStart = maxDistanceFromStart;
        this.startPos = startPos;
    }


}
