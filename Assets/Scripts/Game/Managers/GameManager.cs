﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    #region Fields
    public GameObject Menu;
    [SerializeField] private GameObject Load;
    [SerializeField] private Text loadText;
    public SOPlaces FreePlaces;
    public SOPlaces EnemyPlaces;
    private bool pause = false;
    [SerializeField] private GameObject player;
    [SerializeField] private Text scoreText;
    [SerializeField] private SOGame gameSettings;
    [SerializeField] private SOGame temporyGameSettings;
    private int m_score = 0;

    #endregion

    #region Unity Functions
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Loading());
    }

    private void Update()
    {
        if (player == null)
        {
            GameOver();
            return;
        }
        if (player.transform.position.x > m_score)
        {
            m_score = (int)player.transform.position.x;
            scoreText.text = "Score: " + m_score;
        }
    }

    #endregion

    #region Corutines

    private IEnumerator Loading()
    {
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(0.5f);
            string s = "";
            for (int j = 0; j <= i; j++)
                s += ".";
            loadText.text = "Load" + s;
        }
        Load.SetActive(false);
    }

    private IEnumerator ToGameOver()
    {
        yield return new WaitForSeconds(2f);
        GameSceneManager.Instance.LoadGameOverScene();
    }
    #endregion

    #region Pause Menu Functions

    public void Pause()
    {
        Menu.SetActive(true);
        pause = true;
        Time.timeScale = 0;
    }

    public void ChangePause()
    {
        if (pause)
        {
            Continue();
        }
        else
        {
            Pause();
        }
    }

    public void Continue()
    {
        Menu.SetActive(false);
        pause = false;
        Time.timeScale = 1;
    }

    #endregion




    public void ToMenu()
    {
        Time.timeScale = 1;
        GameSceneManager.Instance.LoadStartScene();
    }

    public void GameOver()
    {
        temporyGameSettings.score = m_score;
        if(gameSettings.score < m_score)
        {
            gameSettings.score = m_score;
            PlayerPrefs.SetInt("Score", m_score);
        }
        StartCoroutine(ToGameOver());
       // GameSceneManager.Instance.LoadGameOverScene();
    }

    
}
