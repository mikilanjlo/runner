﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] public List<SOLevel> levels;
    [SerializeField] public int currentlevel;
    [SerializeField] private int currentStep = 0;
    public static LevelManager instance;

    private void Awake()
    {
        if (instance == null)
        { // Экземпляр менеджера был найден
            instance = this; // Задаем ссылку на экземпляр объекта
        }
        else if (instance == this)
        { // Экземпляр объекта уже существует на сцене
            Destroy(gameObject); // Удаляем объект
        }
    }

    private void Start()
    {
        currentStep = 0;
    }

    public int CurrentStep
    {
        get
        {
            return currentStep;
        }
        set
        {
            currentStep = value;
        }
    }
}
