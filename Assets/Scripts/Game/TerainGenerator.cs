﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerainGenerator : MonoBehaviour
{
    #region Fields
    [SerializeField] private int minDistanceFromPlayer;
    [SerializeField] private int maxCountTerrain;
    [SerializeField] private List<SOTerrain> terrainDatas = new List<SOTerrain>();
    [SerializeField] private GameObject terrainHolder;
    [SerializeField] private int startPos;
    [SerializeField] private SOTerrain startTerrain;
    [SerializeField] private bool AutoGenerate;
    [SerializeField] private List<SOMap> maps;
    [SerializeField] private GameObject FirstWaterLevel;
    [SerializeField] private float FirstWaterLevelHeight;
    private SOMap m_map;
    private LevelManager m_levelManager;


    private List<GameObject> m_currentTerrains = new List<GameObject>();
    private Vector3 m_currentPosition = Vector3.zero;

    #endregion


    #region Unity Functions
    void Start()
    {
        if(maps.Count > 0)
        {
            m_map = maps[Random.Range(0, maps.Count)];
        }
        else
        {
            AutoGenerate = true;
        }
        m_levelManager = terrainHolder.GetComponent<LevelManager>();
        m_currentPosition.x = m_currentPosition.x - startPos;
        for(int i = 0; i < maxCountTerrain; i++)
        {
            SpawnTerrain(Vector3.zero,true);
        }
        maxCountTerrain = m_currentTerrains.Count;
    }

    #endregion

    #region Private Functions
    private void CreateTerrain(GameObject terrainObject, Vector3 position, bool isStart)
    {
        GameObject terrain = Instantiate(terrainObject, position, Quaternion.identity);
        Instantiate(FirstWaterLevel, new Vector3(position.x, position.y + FirstWaterLevelHeight, position.z), Quaternion.identity);
        m_levelManager.CurrentStep++;
        if (m_levelManager.CurrentStep > m_levelManager.levels[m_levelManager.currentlevel].stepsCount)
        {
            m_levelManager.CurrentStep = 0;
            if (m_levelManager.currentlevel + 1 >= m_levelManager.levels.Count)
            {
                m_levelManager.currentlevel = 0;
            }
            else
            {
                m_levelManager.currentlevel++;
            }
        }
        terrain.transform.SetParent(terrainHolder.transform);
        m_currentTerrains.Add(terrain);
        if (!isStart)
            if (m_currentTerrains.Count > maxCountTerrain)
            {
                Destroy(m_currentTerrains[0]);
                m_currentTerrains.RemoveAt(0);
            }
        m_currentPosition.x++;
    }
    #endregion

    #region Public Functions
    public void SpawnTerrain(Vector3 playerPos,bool isStart = false)
    {
        //if (/*(m_currentPosition.x - playerPos.x < minDistanceFromPlayer )||*/ isStart)
        //{
            if (!AutoGenerate)
            { 
                CreateTerrain(m_map.terrains[((int)m_currentPosition.x + startPos) % m_map.terrains.Count], m_currentPosition, isStart);
            }
            else
            {
                int whichTerrain = Random.Range(0, terrainDatas.Count);
                //int countSpawnes;
                //countSpawnes = Random.Range(1, terrainDatas[whichTerrain].maxInSuccesion);
                //for (int i = 0; i < countSpawnes; i++)
                //{
                    if ((int)m_currentPosition.x == 0)
                    {
                        CreateTerrain(startTerrain.GetTerrain(), m_currentPosition, isStart);
                    }
                    else
                    {
                        CreateTerrain(terrainDatas[whichTerrain].GetTerrain(), m_currentPosition, isStart);
                    }    
                //}
            }
        //}
    }

    #endregion

}
